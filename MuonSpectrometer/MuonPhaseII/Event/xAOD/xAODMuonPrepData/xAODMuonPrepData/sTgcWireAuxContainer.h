/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_STGCWIREAUXCONTAINER_H
#define XAODMUONPREPDATA_STGCWIREAUXCONTAINER_H

#include "xAODMuonPrepData/sTgcWireHitFwd.h"
#include "xAODMuonPrepData/versions/sTgcWireAuxContainer_v1.h"
// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::sTgcWireAuxContainer , 1093340389 , 1 )
#endif 